const fs = require('fs'),
  path = require('path'),
  express = require('express'),
  cp = require("child_process"),
  ffmpeg = require("ffmpeg-static")

const PORT = process.env.PORT || 8080
const app = express()

const MAX_FILESIZE = process.env.MAX_FILESIZE || 500

var formats = {
  "mkv": "matroska"
}

var fastFormats = ["mp4", "mkv", "mp3", "wav"]

app.get("/convert", async (req, res) => {
  var file = req.query.file || ""
  var format = req.query.format
  var url = req.query.url

  var filePath = path.join(__dirname, 'downloads', file)

  if (fs.existsSync(filePath)) {
    var ytdlpProcess = cp.spawnSync('yt-dlp', ['--get-filename', url])
    var name = ytdlpProcess.stdout.toString()
    name = name.substring(0, name.lastIndexOf("[") - 1)
    res.setHeader('Content-Disposition', `attachment; filename="${encodeURIComponent(name)}.${format}"`);

    const ffmpegProcess = cp.spawn(ffmpeg, [
      '-i', filePath,
      '-f', formats[format] || format,
      '-movflags', 'frag_keyframe+empty_moov',
      '-preset','ultrafast',
      '-crf', '23',
      '-loglevel', 'error',
      '-'
    ], {
      stdio: [
        'pipe', 'pipe', 'pipe', 'pipe', 'pipe',
      ],
    })

    ffmpegProcess.stderr.setEncoding('utf-8')
    ffmpegProcess.stderr.on('data', (data) => {
      console.log(data)
    })
    // These are debugging lines to watch FFMPEG output :3

    ffmpegProcess.stdio[1].pipe(res)
      .on('close', () => {
        fs.rmSync(filePath)
      })
  }
})

app.get("/download", async (req, res) => {
  const url = req.query.url
  const format = req.query.format || 'mp4'
  const quality = req.query.quality || '720'

  res.setHeader("X-Accel-Buffering", "no")

  var fileName = Math.ceil(Math.random() * 100_000_000_000).toString()
  var videoName = cp.spawnSync('yt-dlp', ['-S', 'res:' + quality, '--get-filename', url]).stdout.toString()

  videoName = videoName.substring(0, videoName.lastIndexOf('.'))
  videoName = videoName.substring(0, videoName.lastIndexOf('['))

  if (!["mp3", "wav", "ogx"].includes(format) && Number(quality) > 720) {
    res.setHeader("Content-Type", "text/html")

    var downloadHTML = fs.readFileSync(path.join(__dirname, 'resources/downloading.html')).toString()

    res.write(downloadHTML.substring(0, downloadHTML.indexOf("{CONTENT}")))

    var filePath = path.join(__dirname, 'downloads', fileName)
    var ytdlpProcess = cp.spawn('yt-dlp', [
      url,
      '-o', filePath,
      '--max-filesize', MAX_FILESIZE + 'm',
      '-S', 'res:' + quality,
      '--no-playlist',
    ])

    var lastDownload = 0

    ytdlpProcess.stderr.setEncoding('utf-8')
    ytdlpProcess.stderr.on('data', (data) => {
      console.log(data)
      res.write(`<div class="error"><p>` + data + `</p></div>`)
    })

    var debounce = false

    ytdlpProcess.stdout.setEncoding('utf-8')
    ytdlpProcess.stdout.on('data', (data) => {
      if (!debounce) {
        if (data.includes("max-filesize")) {
          debounce = true
          res.write(`<p>Uh oh! The video you're trying to download is too large for this server's current settings ${MAX_FILESIZE}. Please try another server? (Visit main page and go to the codeberg for a list of instances!)</p>`)
        }
        else if (data.includes("[download]")) {
          res.write(`<style>#downloading${lastDownload}{ display: none; }</style>`)
          lastDownload += 1
          res.write(`<p id="downloading${lastDownload}">` + data.substring(12) + `</p>`)
        }
      }


    })

    var exited = false

    ytdlpProcess.on('close', () => {

      var files = fs.readdirSync(path.join(__dirname, 'downloads'))
      for (let index = 0; index < files.length; index++) {
        const file = files[index];
        console.log(file)
        if (file.includes(fileName)) {
          fileName = file
          filePath = path.join(__dirname, 'downloads', fileName)
        }
      }
      
      if (exited) {
        if (fs.existsSync(filePath)) {
          fs.rmSync(filePath)
        }
      }
      else if (fs.existsSync(filePath)) {
        res.write(`<iframe src="/convert?file=${fileName}&format=${format}&url=${url}"></iframe>"`)
        res.write(downloadHTML.substring(downloadHTML.indexOf("{CONTENT}") + 9) + `<meta http-equiv="Refresh" content="0; url='/'" />`, () => { res.end() })
      } else {
        res.write("<p>An error has occured!!! We're not exactly sure what the error is, but we cant seem to find the download file. Double check the URL, and if the URL is fine, then file an issue on codeberg. </p>")
      }
    })

    res.on("error", () => {
      if (fs.existsSync(filePath)) {
        fs.rmSync(filePath)
      }
      exited = true
    })
  } else if (["mp3", "wav", "opus"].includes(format)) {
    res.setHeader('Content-Disposition', `attachment; filename="${encodeURIComponent(videoName)}.${format}"`);

    var ytdlpProcess = cp.spawn('yt-dlp', [
      url,
      '-x',
      '-o', '-',
      '--max-filesize', MAX_FILESIZE + 'm',
      '--audio-format', 'mp3',
      '--no-playlist',
    ])

    const ffmpegProcess = cp.spawn(ffmpeg, [
      '-i', 'pipe:3',
      '-f', formats[format] || format,
      '-movflags', 'frag_keyframe+empty_moov',
      '-preset','ultrafast',
      '-crf', '23',
      '-loglevel', 'error',
      '-'
    ], {
      stdio: [
        'pipe', 'pipe', 'pipe', 'pipe', 'pipe',
      ],
    })

    ytdlpProcess.stdout.pipe(ffmpegProcess.stdio[3])

    ytdlpProcess.stderr.setEncoding('utf-8')
    ytdlpProcess.stderr.on('data', (data) => {
      console.log(data)
    })

    ffmpegProcess.stdio[1].pipe(res)
  } else {
    res.setHeader('Content-Disposition', `attachment; filename="${encodeURIComponent(videoName)}.${format}"`);

    var ytdlpProcess = cp.spawn('yt-dlp', [
      url,
      '-o', '-',
      '--max-filesize', MAX_FILESIZE + 'm',
      '-S', 'res:' + quality,
      '--no-playlist',
    ])

    const ffmpegProcess = cp.spawn(ffmpeg, [
      '-i', 'pipe:3',
      '-f', formats[format] || format,
      '-movflags', 'frag_keyframe+empty_moov',
      '-c:a', 'libmp3lame',
      '-preset','ultrafast',
      '-crf', '23',
      '-loglevel', 'error',
      '-'
    ], {
      stdio: [
        'pipe', 'pipe', 'pipe', 'pipe', 'pipe',
      ],
    })

    ytdlpProcess.stdout.pipe(ffmpegProcess.stdio[3])

    ytdlpProcess.stderr.setEncoding('utf-8')
    ytdlpProcess.stderr.on('data', (data) => {
      console.log(data)
    })

    ffmpegProcess.stdio[1].pipe(res)
  }

})

process.on('uncaughtException', (err, origin) => {
  fs.writeSync(
    process.stderr.fd,
    `Caught exception: ${err}\n` +
    `Exception origin: ${origin}`,
  );
});

app.use(express.static(path.join(__dirname, 'static')))

app.listen(PORT, function () {
  console.log("Hosted on port " + PORT)
})